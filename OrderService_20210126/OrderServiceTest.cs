using ExtractAndOverrideTest;
using NUnit.Framework;
using System.Collections.Generic;

namespace OrderService_20210126
{
    public class OrderServiceTest
    {
        private StubOrderService _stubOrderService;
        private Dictionary<string, int> _resultDic;
        private List<Order> _orders;
        [SetUp]
        public void Setup()
        {
            _stubOrderService = new StubOrderService();
            _resultDic = new Dictionary<string, int>();
            _orders = new List<Order>();
        }

        [Test]
        public void Test1()
        {
            _orders.Add(new Order { CustomerName = "Franky", Price = 10, ProductName = "C#", Type = "Book" });
            _orders.Add(new Order { CustomerName = "Chen", Price = 30, ProductName = "C#", Type = "Book" });
            _resultDic.Add( "C#", 40 );
            _stubOrderService.SetOrders(_orders);

            AssertCheck(_resultDic);
        }
        [Test]
        public void Test2()
        {
            _orders.Add(new Order { CustomerName = "Franky", Price = 10, ProductName = "C#", Type = "Book" });
            _orders.Add(new Order{ CustomerName = "Chen", Price = 30, ProductName = "VB", Type = "Book" });
            _resultDic.Add("C#", 10);
            _resultDic.Add("VB", 30);
            _stubOrderService.SetOrders(_orders);

            AssertCheck(_resultDic);
        }
        [Test]
        public void Test3()
        {
            _orders.Add(new Order { CustomerName = "Franky", Price = 10, ProductName = "C#", Type = "Book" });
            _orders.Add(new Order{ CustomerName = "Chen", Price = 30, ProductName = "C#", Type = "Album" });
            _resultDic.Add("C#", 10);
            _stubOrderService.SetOrders(_orders);

            AssertCheck(_resultDic);
        }
        [Test]
        public void Test4()
        {
            _orders.Add(new Order { CustomerName = "Franky", Price = 10, ProductName = "C#", Type = "Book" });
            _orders.Add(new Order { CustomerName = "Chen", Price = 30, ProductName = "C#", Type = "Book" });
            _orders.Add(new Order { CustomerName = "Lynn", Price = 30, ProductName = "VB", Type = "Book" });
            _resultDic.Add("C#", 40);
            _resultDic.Add("VB", 30);
            _stubOrderService.SetOrders(_orders);

            AssertCheck(_resultDic);
        }

        private void AssertCheck(Dictionary<string, int> dic)
        {
            Assert.AreEqual(dic, _stubOrderService.GetTopBooksByOrders());
        }
    }
}