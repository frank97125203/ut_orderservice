﻿using ExtractAndOverrideTest;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderService_20210126
{
    internal class StubOrderService : OrderService
    {
        private List<Order> _orders = new List<Order>();

        internal void SetOrders(List<Order> orders)
        {
            this._orders = orders;
        }
        protected override IEnumerable<Order> GetOrders()
        {
            return this._orders;
        }
    }
}
